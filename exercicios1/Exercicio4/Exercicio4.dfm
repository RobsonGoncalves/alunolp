﻿object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 518
  ClientWidth = 752
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Lista1: TListBox
    Left = 392
    Top = 40
    Width = 337
    Height = 449
    ItemHeight = 13
    TabOrder = 0
  end
  object Edit1: TEdit
    Left = 48
    Top = 40
    Width = 305
    Height = 25
    TabOrder = 1
    Text = 'Edit1'
  end
  object BotãoAdd: TButton
    Left = 48
    Top = 88
    Width = 89
    Height = 33
    Caption = 'Adicionar'
    TabOrder = 2
    OnClick = BotãoAddClick
  end
  object BotãoExcluir: TButton
    Left = 160
    Top = 88
    Width = 81
    Height = 33
    Caption = 'Excluir'
    TabOrder = 3
    OnClick = BotãoExcluirClick
  end
  object BotãoLimpar: TButton
    Left = 264
    Top = 88
    Width = 89
    Height = 33
    Caption = 'Limpar'
    TabOrder = 4
    OnClick = BotãoLimparClick
  end
  object BotãoEditar: TButton
    Left = 48
    Top = 136
    Width = 89
    Height = 33
    Caption = 'Editar'
    TabOrder = 5
    OnClick = BotãoEditarClick
  end
end
