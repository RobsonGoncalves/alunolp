unit Exercicio4;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    Lista1: TListBox;
    Edit1: TEdit;
    BotãoAdd: TButton;
    BotãoExcluir: TButton;
    BotãoLimpar: TButton;
    BotãoEditar: TButton;
    procedure BotãoAddClick(Sender: TObject);
    procedure BotãoExcluirClick(Sender: TObject);
    procedure BotãoLimparClick(Sender: TObject);
    procedure BotãoEditarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.BotãoAddClick(Sender: TObject);
begin
  Lista1.Items.Add(Edit1.Text);
  if (Edit1.text = '') then
  begin
    ShowMessage('O campo esta vazio');
  end;

end;

procedure TForm1.BotãoEditarClick(Sender: TObject);
begin
  Lista1.Items[Lista1.ItemIndex] := (Edit1.Text);
end;

procedure TForm1.BotãoExcluirClick(Sender: TObject);
begin
  Lista1.DeleteSelected;
end;

procedure TForm1.BotãoLimparClick(Sender: TObject);
begin
  Lista1.Clear;
end;

end.
