object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 443
  ClientWidth = 447
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Lb_Nome: TLabel
    Left = 40
    Top = 40
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Lb_prof: TLabel
    Left = 40
    Top = 91
    Width = 44
    Height = 13
    Caption = 'Profissao'
  end
  object Lb_Nivel: TLabel
    Left = 40
    Top = 139
    Width = 23
    Height = 13
    Caption = 'Nivel'
  end
  object Lb_Ataque: TLabel
    Left = 40
    Top = 187
    Width = 35
    Height = 13
    Caption = 'Ataque'
  end
  object Lb_Defesa: TLabel
    Left = 40
    Top = 235
    Width = 34
    Height = 13
    Caption = 'Defesa'
  end
  object Lb_Vida: TLabel
    Left = 40
    Top = 285
    Width = 20
    Height = 13
    Caption = 'Vida'
  end
  object Ed_Nome: TEdit
    Left = 40
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Ed_Prof: TEdit
    Left = 40
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Ed_Nivel: TEdit
    Left = 40
    Top = 160
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Ed_Ataque: TEdit
    Left = 40
    Top = 208
    Width = 121
    Height = 21
    TabOrder = 3
  end
  object Ed_Defesa: TEdit
    Left = 40
    Top = 256
    Width = 121
    Height = 21
    TabOrder = 4
  end
  object Ed_Vida: TEdit
    Left = 40
    Top = 304
    Width = 121
    Height = 21
    TabOrder = 5
  end
  object BotaoInserir: TButton
    Left = 264
    Top = 79
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 6
    OnClick = BotaoInserirClick
  end
  object Button2: TButton
    Left = 264
    Top = 110
    Width = 75
    Height = 25
    Caption = 'Button2'
    TabOrder = 7
  end
  object NetHTTPClient1: TNetHTTPClient
    Asynchronous = False
    ConnectionTimeout = 60000
    ResponseTimeout = 60000
    AllowCookies = True
    HandleRedirects = True
    UserAgent = 'Embarcadero URI Client/1.0'
    Left = 352
    Top = 80
  end
end
