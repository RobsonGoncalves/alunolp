unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent,Unit2, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    ListaPerso: TListBox;
    Botao1: TButton;
    NetHTTPClient1: TNetHTTPClient;
    Botao2: TButton;
    Lb_Descri��o: TLabel;
    procedure Botao1Click(Sender: TObject);
    procedure ListaPersoDblClick(Sender: TObject);
    procedure Botao2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  TPersonagem = class(TObject)
    Nome : String;
    Profissao : String;
    Nivel  :Integer;
    Ataque :Integer;
    Defesa : Integer;
    Vida : Integer;
  end;

var
  Form1: TForm1;
  personagem : TPersonagem;
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm1.Botao1Click(Sender: TObject);
var conteudo : string;
ListaAtributo : TArray<string>;
ListaPersonagem : TArray<string>;
stringPersonagem : string;


begin
 conteudo := NetHTTPClient1.Get('https://venson.net.br/ws/personagens/').ContentAsString();
 ListaPersonagem := conteudo.Split(['&']);

 for stringPersonagem in ListaPersonagem do
 begin
   ListaAtributo := stringPersonagem.Split([';']);
   personagem := TPersonagem.Create;

   personagem.Nome := ListaAtributo[0];
   personagem.Profissao := ListaAtributo[1];
   personagem.Nivel := ListaAtributo[2].ToInteger;
   personagem.Ataque := ListaAtributo[3].ToInteger;
   personagem.Defesa := ListaAtributo[4].ToInteger;
   personagem.Vida := ListaAtributo[5].ToInteger;

   ListaPerso.Items.AddObject(personagem.Nome, personagem);
 end;



end;

procedure TForm1.Botao2Click(Sender: TObject);
begin
  Form2 := TForm2.Create(Application);
  Form2.Show;
end;

procedure TForm1.ListaPersoDblClick(Sender: TObject);
begin
  personagem := TPersonagem(ListaPerso.Items.Objects[ListaPerso.ItemIndex]);
  ShowMessage('Seu Personagem � ' + personagem.Nome +' '+ personagem.Profissao +' '+ personagem.Nivel.ToString +'Nv '+ personagem.Ataque.ToString +'Atq '+ personagem.Defesa.ToString +'Def '+ personagem.Vida.ToString +'Vida');
end;

end.
