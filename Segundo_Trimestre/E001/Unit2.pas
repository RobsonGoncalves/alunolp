unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    Memo1: TMemo;
    Memo2: TMemo;
    Botao1: TButton;
    Botao2: TButton;
    Botao3: TButton;
    Botao4: TButton;
    Botao5: TButton;
    Botao6: TButton;
    procedure Botao1Click(Sender: TObject);
    procedure Botao2Click(Sender: TObject);
    procedure Botao5Click(Sender: TObject);
    procedure Botao4Click(Sender: TObject);
    procedure Botao3Click(Sender: TObject);
    procedure Botao6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var c :String;
  Form2: TForm2;

implementation

{$R *.dfm}

procedure TForm2.Botao1Click(Sender: TObject);
begin
 c:= Memo1.Text;
 Memo2.Text := c.Length.ToString;

end;

procedure TForm2.Botao2Click(Sender: TObject);
begin
 c:= Memo1.Text;
 if c.Contains('s') then
   Begin
    Memo2.Text := ('Cont�m a letra S');
   End
 Else
   Begin
    Memo2.Text := ('N�o h� S');
   End;
end;

procedure TForm2.Botao3Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := c.Trim;
end;

procedure TForm2.Botao4Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := LowerCase(c);
end;

procedure TForm2.Botao5Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := UpperCase(c);
end;

procedure TForm2.Botao6Click(Sender: TObject);
begin
  c:= Memo1.Text;
  Memo2.Text := c.Replace('c','i');
end;

end.
