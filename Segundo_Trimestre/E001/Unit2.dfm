object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 198
  ClientWidth = 362
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 129
    Height = 180
    Lines.Strings = (
      'Memo1')
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 224
    Top = 8
    Width = 129
    Height = 182
    Lines.Strings = (
      'Memo2')
    TabOrder = 1
  end
  object Botao1: TButton
    Left = 143
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Length'
    TabOrder = 2
    OnClick = Botao1Click
  end
  object Botao2: TButton
    Left = 143
    Top = 39
    Width = 75
    Height = 25
    Caption = 'Contains'
    TabOrder = 3
    OnClick = Botao2Click
  end
  object Botao3: TButton
    Left = 143
    Top = 70
    Width = 75
    Height = 25
    Caption = 'Trim'
    TabOrder = 4
    OnClick = Botao3Click
  end
  object Botao4: TButton
    Left = 143
    Top = 101
    Width = 75
    Height = 25
    Caption = 'Lower'
    TabOrder = 5
    OnClick = Botao4Click
  end
  object Botao5: TButton
    Left = 143
    Top = 132
    Width = 75
    Height = 25
    Caption = 'Upper'
    TabOrder = 6
    OnClick = Botao5Click
  end
  object Botao6: TButton
    Left = 143
    Top = 165
    Width = 75
    Height = 25
    Caption = 'Replace'
    TabOrder = 7
    OnClick = Botao6Click
  end
end
